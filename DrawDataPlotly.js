import throttle from "lodash/throttle.js";
import IconBlur from "./icons/material/blur_on_black_24dp.svg";
import IconCreate from "./icons/material/create_black_24dp.svg";
import IconGrain from "./icons/material/grain_black_24dp.svg";
import IconClean from "./icons/material/cleaning_services_black_24dp.svg";

export const TOOLS = {
    "single": {
        icon: IconCreate,
        cursor: "url(\"" + IconCreate + "\") 4 20, pointer"
    },
    "gaussian": {
        icon: IconBlur,
        cursor: "url(\"" + IconBlur + "\") 12 12, pointer"
    },
    "uniform": {
        icon: IconGrain,
        cursor: "url(\"" + IconGrain + "\") 12 12, pointer"
    },
    "erase": {
        icon: IconClean,
        cursor: "url(\"" + IconClean + "\") 10 20, crosshair"
    }
}

const PLOT_CONFIG = {
    doubleClick: false, displayModeBar: false, staticPlot: false, editable: false, edits: null
}

export class DrawDataPlotly {
    _plot = undefined;
    _dataLayer = undefined;
    _drawTool = { tool: "single", opts: { trace: 0 } };
    _circleShape = { type: "circle", xref: "x", yref: "y", line: { width: 1, color: "black", dash: "dash" } };
    _generateSamplesFn = undefined;
    _hoverFn = undefined;
    _originalConfig = undefined;
    _isEnabled = false;

    constructor(plot, enable = true) {
        this._plot = plot;
        if (enable) {
            this.enable();
        }
    }

    isEnabled() {
        return this._isEnabled;
    }

    getPlot() {
        return this._plot;
    }

    enable() {
        if (this._isEnabled) {
            throw 'PlotlyController is already enabled';
        }
        // Save original config and set required options
        this._originalConfig = this._plot._context;
        const config = Object.assign({}, this._plot._context, PLOT_CONFIG);
        Plotly.react(this._plot, this._plot.data, this._plot.layout, config);

        // Get the plot canvas for hover and click events
        this._dataLayer = this._plot
            .getElementsByClassName("xy")[0]
            .getElementsByClassName("nsewdrag")[0];
        this._isEnabled = true;
    }

    disable() {
        this._removeShapes();
        this._configureToolNone();
        // Restore the original config
        Plotly.react(this._plot, this._plot.data, this._plot.layout, this._originalConfig);
        this._originalConfig = undefined;
        this._isEnabled = false;
    }

    setTool(newTool) {
        if (!this._isEnabled) {
            throw 'Controller must be enabled.'
        }
        this._drawTool = newTool;
        if (!this._plot) {
            return;
        }
        this._removeShapes();
        if (TOOLS.hasOwnProperty(this._drawTool.tool)) {
            Plotly.d3.select(this._dataLayer).style('cursor', TOOLS[this._drawTool.tool].cursor);
        }
        switch (this._drawTool.tool) {
            case "single":
                this._configureToolSingle();
                break;
            case "gaussian":
                this._configureToolGaussian();
                break;
            case "uniform":
                this._configureToolUniform();
                break;
            case "erase":
                this._confgureToolErase();
                break;
            default:
                this._configureToolNone();
                return false; // Unknown tool
        }
        return true; // All good
    }

    _removeListeners() {
        this._plot.removeAllListeners("plotly_selected");
        const pd3 = Plotly.d3.select(".plotly");
        pd3.on("mousedown", undefined);
        pd3.on("mousemove", undefined);
        pd3.on("mouseleave", undefined);
    }

    _configureToolNone() {
        this._removeListeners();
        Plotly.relayout(this._plot, { dragmode: "pan" });
        Plotly.d3.select(this._dataLayer).style('cursor', null);
    }

    _configureToolSingle() {
        this._removeListeners();
        Plotly.d3.select(".plotly").on("mousedown", this._onPlotClick.bind(this));
        this._generateSamplesFn = (x, y) => ({ x: [x], y: [y] });
        Plotly.relayout(this._plot, { dragmode: 'pan' });
    }

    _configureToolGaussian() {
        this._plot.removeAllListeners("plotly_selected");
        const pd3 = Plotly.d3.select(".plotly");
        pd3.on("mousedown", this._onPlotClick.bind(this));
        pd3.on("mousemove", this._onPlotHover.bind(this));
        pd3.on("mouseleave", this._removeShapes.bind(this));
        this._hoverFn = throttle(
            this._mkHoverCirclePlacer(2 * Math.sqrt(this._drawTool.opts.variance)),
            70
        );
        this._generateSamplesFn = (x, y) =>
            sample2DNormal(x, y, this._drawTool.opts.variance, 32);
        Plotly.relayout(this._plot, { dragmode: 'pan' });
    }

    _configureToolUniform() {
        this._plot.removeAllListeners("plotly_selected");
        const pd3 = Plotly.d3.select(".plotly");
        pd3.on("mousedown", this._onPlotClick.bind(this));
        pd3.on("mousemove", this._onPlotHover.bind(this));
        pd3.on("mouseleave", this._removeShapes.bind(this));
        this._hoverFn = throttle(
            this._mkHoverCirclePlacer(this._drawTool.opts.radius),
            70
        );
        this._generateSamplesFn = (x, y) =>
            sampleUniformBall(x, y, this._drawTool.opts.radius, 32);
        Plotly.relayout(this._plot, { dragmode: 'pan' });
    }

    _confgureToolErase() {
        this._removeListeners();
        this._plot.on("plotly_selected", this._onPlotSelect.bind(this));
        Plotly.relayout(this._plot, { dragmode: "lasso" });
    }

    _onPlotSelect(d) {
        this._plot.data.forEach((trace, tIdx) => {
            if (!trace.selectedpoints) {
                return;
            }
            const selected = trace.selectedpoints.slice();
            selected.sort((a, b) => a - b);
            while (selected.length) {
                const pIdx = selected.pop();
                trace.x.splice(pIdx, 1);
                trace.y.splice(pIdx, 1);
            }
            trace.selectedpoints = undefined;
        });

        Plotly.react(this._plot, this._plot.data, this._plot.layout);
    }

    _onPlotClick(d, i) {
        const e = Plotly.d3.event;
        if (e.target != this._dataLayer) {
            return;
        }
        if (
            this._plot.layout.xaxis.type != "linear" ||
            this._plot.layout.yaxis.type != "linear"
        ) {
            throw "Drawing is implemented only for linear axes.";
        }
        const t = e.target.attributes;
        const x = (e.offsetX - t.x.value) / t.width.value;
        const y = 1 - (e.offsetY - t.y.value) / t.height.value;
        const xRange = this._plot.layout.xaxis.range;
        const yRange = this._plot.layout.yaxis.range;
        const dataX = xRange[0] + x * (xRange[1] - xRange[0]);
        const dataY = yRange[0] + y * (yRange[1] - yRange[0]);
        const samples = this._generateSamplesFn(dataX, dataY);
        Plotly.extendTraces(this._plot, { x: [samples.x], y: [samples.y] }, [
            this._drawTool.opts.trace,
        ]);
    }

    _onPlotHover(d, i) {
        const e = Plotly.d3.event;
        if (!this._hoverFn) {
            return;
        }
        if (e.target != this._dataLayer) {
            return;
        }
        const t = e.target.attributes;
        const x = (e.offsetX - t.x.value) / t.width.value;
        const y = 1 - (e.offsetY - t.y.value) / t.height.value;
        const xRange = this._plot.layout.xaxis.range;
        const yRange = this._plot.layout.yaxis.range;
        const dataX = xRange[0] + x * (xRange[1] - xRange[0]);
        const dataY = yRange[0] + y * (yRange[1] - yRange[0]);
        this._hoverFn(dataX, dataY);
    }

    _removeShapes() {
        if (this._hoverFn) {
            this._hoverFn.cancel();
        }
        if (!this._plot.layout.shapes || this._plot.layout.shapes.length == 0) {
            return;
        }
        const idx = this._plot.layout.shapes.indexOf(this._circleShape);
        if (idx == -1) {
            return;
        }
        this._plot.layout.shapes.splice(idx, 1);
        Plotly.relayout(this._plot, this._plot.layout);
    }

    _mkHoverCirclePlacer(radius) {
        return (x, y) => {
            this._circleShape.x0 = x - radius;
            this._circleShape.y0 = y - radius;
            this._circleShape.x1 = x + radius;
            this._circleShape.y1 = y + radius;
            if (!this._plot.layout.shapes) {
                this._plot.layout.shapes = [];
            }
            if (this._plot.layout.shapes.indexOf(this._circleShape) == -1) {
                this._plot.layout.shapes.push(this._circleShape);
            }
            Plotly.relayout(this._plot, this._plot.layout);
        };
    }

}

function sampleUniformBall(centerX, centerY, radius, nSamples) {
    const dist = Array.from({ length: nSamples }, () => Math.random());
    const angles = Array.from(
        { length: nSamples },
        () => Math.random() * 2 * Math.PI
    );
    const x = dist.map(
        (d, i) => centerX + Math.sqrt(d) * radius * Math.cos(angles[i])
    );
    const y = dist.map(
        (d, i) => centerY + Math.sqrt(d) * radius * Math.sin(angles[i])
    );
    return { x: x, y: y };
}

function sample2DNormal(centerX, centerY, variance, nSamples) {
    // Shameless copy-paste from stackoverflow (25582882, Maxwell Collard).
    // Standard Normal variate using Box-Muller transform.
    function randn_bm() {
        let u = 0,
            v = 0;
        while (u === 0) u = Math.random(); // Convert [0,1) to (0,1)
        while (v === 0) v = Math.random();
        return Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
    }
    const sqrtVar = Math.sqrt(variance);
    // const sqrtVar = variance
    const x = Array.from(
        { length: nSamples },
        () => centerX + randn_bm() * sqrtVar
    );
    const y = Array.from(
        { length: nSamples },
        () => centerY + randn_bm() * sqrtVar
    );
    return { x: x, y: y };
}

